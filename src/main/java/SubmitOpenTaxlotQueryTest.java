
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import com.google.longrunning.GetOperationRequest;
import com.google.longrunning.Operation;
import com.google.longrunning.OperationsGrpc;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Timestamp;

import com.arcesium.nautilus.v1.model.EngineKnowledgeTime;
import com.arcesium.nautilus.v1.model.EventTime;
import com.arcesium.nautilus.v1.model.enums.AccountingMode;
import com.arcesium.nautilus.v1.service.EventTimeFilter;
import com.arcesium.nautilus.v1.service.FinancialCalculationOptions;
import com.arcesium.nautilus.v1.service.OpenTaxLotQueryRequest;
import com.arcesium.nautilus.v1.service.QueryOperationResponse;
import com.arcesium.nautilus.v1.service.QueryParameters;
import com.arcesium.nautilus.v1.service.QueryServiceGrpc;
import com.arcesium.nautilus.v1.service.QueryStatus;

import io.grpc.CallCredentials;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.Status;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Client test class for {@link OpenTaxLotQueryRequest}
 *
 * @author arcesium-goyaraja
 */
public class SubmitOpenTaxlotQueryTest {

    public static void main(String[] args) throws InterruptedException, InvalidProtocolBufferException {

        ManagedChannel channel = ManagedChannelBuilder.forAddress("ec2-52-91-178-37.compute-1.amazonaws.com", 7817)
                .usePlaintext()
                .build();
        // ManagedChannel channel = ManagedChannelBuilder
        // .forAddress("nlb-nautilusdev-pod-external" + "-648661229823f720.elb.us-east-1.amazonaws.com", 80)
        // .usePlaintext()
        // .build();

        Metadata headers = new Metadata();
        Metadata.Key<String> key = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);
        headers.put(key, "Bearer " + getJWT());
        CallCredentials callCredentials = new CallCredentials() {
            @Override
            public void applyRequestMetadata(RequestInfo requestInfo, Executor appExecutor, MetadataApplier applier) {
                appExecutor.execute(() -> {
                    try {
                        applier.apply(headers);
                    } catch (Throwable e) {
                        applier.fail(Status.UNAUTHENTICATED.withCause(e));
                    }
                });
            }

            @Override
            public void thisUsesUnstableApi() {
            }
        };

        final QueryServiceGrpc.QueryServiceBlockingStub queryServiceBlockingStub = QueryServiceGrpc
                .newBlockingStub(channel)
                .withCallCredentials(callCredentials);
        OpenTaxLotQueryRequest openTaxLotQueryRequest = getOpenTaxlotQueryRequest();
        final Operation queryOperation = queryServiceBlockingStub.submitOpenTaxLotQuery(openTaxLotQueryRequest);
        final String requestId = queryOperation.getName();

        final OperationsGrpc.OperationsBlockingStub operationsBlockingStub = OperationsGrpc.newBlockingStub(channel)
                .withCallCredentials(callCredentials);

        GetOperationRequest getOperationRequest = GetOperationRequest.newBuilder().setName(requestId).build();

        // Restricting the whole run for 20 sec
        long t = System.currentTimeMillis();
        long end = t + 20000;

        Operation operation = null;
        while (System.currentTimeMillis() < end) {
            operation = operationsBlockingStub.getOperation(getOperationRequest);
            final boolean done = operation.getDone();
            if (done) {
                break;
            }
            TimeUnit.SECONDS.sleep(2);
        }
        System.out.println("Hello");

        Any response = operation.getResponse();

        final ByteString value = response.getValue();

        final QueryOperationResponse queryOperationResponse = QueryOperationResponse.parseFrom(value);
        System.out.println("Client Request Id : " + queryOperationResponse.getClientRequestId());
        final QueryStatus queryStatus = queryOperationResponse.getQueryStatus();
        System.out.println(queryStatus);
    }

    private static OpenTaxLotQueryRequest getOpenTaxlotQueryRequest() {

        String clientRequestID = UUID.randomUUID().toString();
        String bookId = "70";

        FinancialCalculationOptions queryOptions = FinancialCalculationOptions.newBuilder()
                .setAccountingMode(AccountingMode.ACCOUNTING_MODE_BUNDLE)
                .build();

        long millis = Instant.now().toEpochMilli();
        Timestamp currentTimestamp = Timestamp.newBuilder()
                .setSeconds(millis / 1000)
                .setNanos((int) ((millis % 1000) * 1000000))
                .build();

        QueryParameters queryParameters = QueryParameters.newBuilder()
                .setClientRequestId(clientRequestID)
                .setBookId(bookId)
                .setQueryOptions(queryOptions)
                .build();

        EventTimeFilter eventTimeFilter = EventTimeFilter.newBuilder()
                .addEventTimes(EventTime.newBuilder().setTime(currentTimestamp).build())
                .setKnowledgeTime(EngineKnowledgeTime.newBuilder().setTime(currentTimestamp).build())
                .build();

        return OpenTaxLotQueryRequest.newBuilder()
                .setQueryParameters(queryParameters)
                .setTimeFilter(eventTimeFilter)
                .build();
    }

    private static String getJWT() {
        final String PUBLIC_KEY_STRING = "MIGbMBAGByqGSM49AgEGBSuBBAAjA4GGAAQBbPfD7l14SfXo/PlwKd0T3eXPlSD3Hfocfn/1rnIyXY22zCOjN5L3d06nDH6QvzSU99G1u6kFE4osAzxcO3mNiG4Ahbod2jwkZQHURAItYvzKNq36ek6y00v7QBdYqFHX6N4iQIr573HV9jfqpRmfQ8eqYbXnZ1bqkttcsZOCxv463s0=";
        final String PRIVATE_KEY_STRING = "MF8CAQAwEAYHKoZIzj0CAQYFK4EEACMESDBGAgEBBEFmBsSrOOjJL6KMwW4G7JO7PSI0NnwD3iuS0Fu/HjTgSmeF6pjserQexJtHMk6UeFXdNQ3CUCcmH4oz7i5YeWHaxg==";

        ECPrivateKey privateKey = null;
        try {
            KeyFactory kf = KeyFactory.getInstance("EC");
            byte[] decodedKey = Base64.getDecoder().decode(PRIVATE_KEY_STRING);
            PKCS8EncodedKeySpec pk = new PKCS8EncodedKeySpec(decodedKey);

            privateKey = (ECPrivateKey) kf.generatePrivate(pk);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        Instant now = Instant.now();
        Date issuedAt = Date.from(now);

        return Jwts.builder()
                .signWith(privateKey, SignatureAlgorithm.ES512)
                .setIssuedAt(issuedAt)
                .setSubject("dummy-user")
                .setIssuer("Arcesium")
                .setAudience("adapter")
                .setExpiration(Date.from(now.plusSeconds(TimeUnit.HOURS.toSeconds(1))))
                .compact();
    }
}
